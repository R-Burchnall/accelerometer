import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Chart } from 'chart.js';
import { File } from '@ionic-native/file';
import { DeviceMotion, DeviceMotionAccelerationData } from '@ionic-native/device-motion';
import { FilePath } from '@ionic-native/file-path';
import { ToastController } from 'ionic-angular';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild('MyChart') barCanvas;
  @ViewChild('StartTime') TXT_Start;
  @ViewChild('RecordedValues') TXT_Total;

  barChart: any;
  records: any;
  labels: any;

  Update: any;

  Path: string;
  Dir: string;

  constructor(public navCtrl: NavController,
    public file: File,
    private toastCtrl: ToastController,
    private motion: DeviceMotion,
    private filePath: FilePath) {

    this.initaliseRecords();

    this.labels = [];
    for (var i = 30; i > 0; i--) {
      this.labels.push((-i).toString());
    }

    this.Path = this.file.externalApplicationStorageDirectory;
    console.log(this.file);
    this.Dir = 'Accelerometer';
  }

  ionViewWillLoad(){
    this.UpdateGraph();
    this.UpdateText();

    this.Update = this.motion.watchAcceleration({frequency: 1000}).subscribe((acceleration: DeviceMotionAccelerationData) => {
      this.records.ValuesX.push(acceleration.x);
      this.records.ValuesY.push(acceleration.y);
      this.records.ValuesZ.push(acceleration.z);

      this.UpdateGraph();
      this.UpdateText();
    });
  }

  initaliseRecords(){
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date + ' ' + time;

    this.records = {
      StartTime: dateTime,
      duration: 0,
      ValuesX: [],
      ValuesY: [],
      ValuesZ: []
    }
  }

  UpdateText() {
    this.TXT_Start.nativeElement.innerText = "Start Time: " + this.records.StartTime;
    this.TXT_Total.nativeElement.innerText = "- Total Records: " + this.records.ValuesX.length.toString();
  }

  UpdateGraph() {
    var start = this.records.ValuesX.length - 30 <= 0 ? 0 : this.records.ValuesX.length - 30;
    var end = this.records.ValuesX.length;

    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: 'line',
      data: {
        labels: this.labels,
        datasets: [{
          label: 'X values',
          data: this.records.ValuesX.slice(start, end),
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
          ],
          borderColor: [
            'rgba(255,99,132,1)',
          ],
          borderWidth: 1
        },
        {
          label: 'Y values',
          data: this.records.ValuesY.slice(start, end),
          backgroundColor: [
            'rgba(54, 162, 235, 0.2)',
          ],
          borderColor: [
            'rgba(54, 162, 235, 1)',
          ],
          borderWidth: 1
        }, {
          label: 'Z values',
          data: this.records.ValuesZ.slice(start, end),
          backgroundColor: [
            'rgba(255, 206, 86, 0.2)',
          ],
          borderColor: [
            'rgba(255, 206, 86, 1)',
          ],
          borderWidth: 1
        }]
      },
      options: {
        animation: false,
        elements: {
          lineTension: 0,
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }

    });
  }

  Clear() {
    this.initaliseRecords();
    this.UpdateGraph();
    this.UpdateText();
    this.presentToast("Cleared any recorded values.");
  }

  Save() {
    this.CheckForDirectoryExistance();
    this.AttemptToWriteFile();
  }

  CheckForDirectoryExistance() {
    this.file.checkDir(this.Path, this.Dir)
      .then(_ => console.log('Directory exists'))
      .catch(err => {
        console.log(this.Path + this.Dir, ': Creating directory...')
        this.file.createDir(this.Path, this.Dir, false)
      });
  }

  AttemptToWriteFile() {
    var filename = this.records.StartTime.replace(/[: ]/g, '-') + ".json"
    console.log("Attempting to save to: ", this.Path + this.Dir + "/" + filename);

    this.file.writeFile(this.Path + this.Dir + "/", filename, JSON.stringify(this.records))
        .then(_ => this.presentToast("Successfully created file at: " + this.Path + this.Dir + "/" + filename))
        .catch(error => {
          console.log("There was an error trying to write into the file", error)
          this.presentToast(error.message);
        });
  }

  presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }
}
